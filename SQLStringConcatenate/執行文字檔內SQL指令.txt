﻿<1>SQL Server
sqlcmd -S 127.0.0.1 -d AdventureWorks2014 -i EXEC.sql -o EXECRESULT.log 

<2>DB2
db2 connect to YourDatabase user YourDBUser using YourDBPassword
db2 -tvf EXEC.sql -l EXECRESULT.log 
db2 connect reset