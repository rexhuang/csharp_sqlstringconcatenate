﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SQLStringConcatenate
{
    public partial class Form1 : Form
    {
        string exePath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
 

            string fileOutputText = "";
            StreamReader fileInput;

            if (!File.Exists(exePath + @"\INPUT.txt"))
            {
                MessageBox.Show("找不到來源檔INPUT.txt");
                return;
            }
            else {
                fileInput = new StreamReader(exePath + @"\INPUT.txt");
            }


            do
            {
                string tempText = "";
                string tableName = fileInput.ReadLine();
                tempText = richTextBoxSQL.Text + "\r\n";
                tempText = tempText.Replace("[TABLENAME]", tableName);


                fileOutputText = fileOutputText + tempText;
                System.IO.File.WriteAllText(exePath + @"\OUTPUT.sql", fileOutputText);

            } while (fileInput.Peek() != -1);

            fileInput.Close();
            MessageBox.Show("結果檔已經產生");
        }



        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                richTextBoxSQL.Text = listBox1.SelectedItem.ToString();
            }
            else {
                MessageBox.Show("選取失敗!");
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                richTextBoxSQL.Text = listBox2.SelectedItem.ToString();
            }
            else {
                MessageBox.Show("選取失敗!");
            }
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox3.SelectedItem != null)
            {
                richTextBoxSQL.Text = listBox3.SelectedItem.ToString();
            }
            else {
                MessageBox.Show("選取失敗!");
            }
        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox4.SelectedItem != null)
            {
                richTextBoxSQL.Text = listBox4.SelectedItem.ToString();
            }
            else {
                MessageBox.Show("選取失敗!");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}
