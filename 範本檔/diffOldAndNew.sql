-- ***** 這份指令只比對欄位名稱，沒有比對欄位型態 *****
-- 用途：新舊系統資料庫欄位不一致比對
--將資料庫所有Table個別存成sql檔
--再使用:r sql\TableA.sql、:r sql\TableB.sql、:r sql\TableC.sql去跑出結果
--sqlcmd -S 資料庫位址 -U 資料庫帳號 -P 資料庫密碼 -h-1 -i diffOldAndNewRUN.sql -o diffOldAndNew.log

SET NOCOUNT ON; 
GO

DECLARE @DB1Name VARCHAR(MAX) = '舊系統資料庫'; 
DECLARE @DB2Name VARCHAR(MAX) = '新系統資料庫';     
DECLARE @DB1TableName VARCHAR(MAX) = '[TABLENAME]';
DECLARE @DB2TableName VARCHAR(MAX) = @DB1TableName;
DECLARE @SQL VARCHAR(MAX);
DECLARE @RESULT VARCHAR(MAX);
DECLARE @NEWLINE AS CHAR(2) = CHAR(13) + CHAR(10)

PRINT '【'+@DB1TableName+'舊系統資料庫才有的欄位】'
SET @SQL = '
Select COLUMN_NAME AS '+@DB1Name+'才有的欄位 From '+ @DB1Name +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = '''+@DB1TableName+'''
EXCEPT 
Select COLUMN_NAME AS '+@DB2Name+' From '+ @DB2Name +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = '''+@DB2TableName+'''
'
EXEC(@SQL);

PRINT @NEWLINE

PRINT '【'+@DB1TableName+'新系統資料庫才有的欄位】'
SET @SQL = '
Select COLUMN_NAME AS '+@DB2Name+'才有的欄位 From '+ @DB2Name +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = '''+@DB2TableName+'''
EXCEPT 
Select COLUMN_NAME AS '+@DB1Name+' From '+ @DB1Name +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = '''+@DB1TableName+'''
'
EXEC(@SQL)

PRINT @NEWLINE
PRINT '========================'
PRINT @NEWLINE
PRINT @NEWLINE
 

SET NOCOUNT OFF; 
GO

